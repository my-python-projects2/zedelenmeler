from django import forms

from pr_app.models import ZedelenmelerinQeydiyyatiJurnali


class ZedelenmelerinQeydiyyatiForm(forms.ModelForm):
    # profile_picture = forms.ImageField(required=False, error_messages={'invalid': ('Image filed only')},
    #                                    widget=forms.FileInput)

    class Meta:
        model = ZedelenmelerinQeydiyyatiJurnali
        fields = ('abonent', 'melumatin_daxil_oldugu_vaxt', 'melumatin_mezmunu', 'olculmus_zedenin_xususiyyeti',
                  'temire_verilme_vaxti', 'kime_temire_verilib','temir_edilib','kim_terefinden_temir_edilib','qeyd')

    def __init__(self, *args, **kwargs):
        super(ZedelenmelerinQeydiyyatiForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'