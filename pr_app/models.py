from django.db import models


class Idare(models.Model):
    adi = models.CharField(max_length=10, blank=True)
    yerlesdiyi_rayon = models.CharField(max_length=11, blank=True)
    telefon_nomresi = models.CharField(max_length=11, blank=True)

    def __str__(self):
        return self.adi

    class Meta:
        verbose_name = 'İdarə'
        verbose_name_plural = 'İdarələr'


class Montyor(models.Model):
    adi = models.CharField(max_length=10, blank=True)
    idare = models.ForeignKey(Idare, on_delete=models.CASCADE, blank=True)
    tehkim_olunmus_paylayici_skaflar = models.ManyToManyField('PaylayiciSkaf', through='Tehkimolunma',
                                                              through_fields=('montyor', 'paylayiciskaf',), blank=True)

    def __str__(self):
        return self.adi

    class Meta:
        verbose_name = 'Montyor'
        verbose_name_plural = 'Montyorlar'


class PaylayiciSkaf(models.Model):
    STATUS_CHOICES = [
        ('tps', 'tps'),
        ('ops', 'ops'),
    ]

    skafin_nomresi = models.CharField(max_length=11, blank=True)
    mudafie_xetti = models.CharField(max_length=10, blank=True)
    novu = models.CharField(max_length=12, choices=STATUS_CHOICES, blank=True)
    magistral = models.CharField(max_length=10, blank=True)
    kabel_nomresi = models.ManyToManyField('KabelNomreleri', through='Tehkimolunma',
                                           through_fields=('paylayiciskaf', 'kabel_nomreleri'), blank=True)
    bolme = models.CharField(max_length=20, blank=True)
    qeyd = models.CharField(max_length=50, blank=True)
    tabe_oldugu_idare = models.ForeignKey(Idare, on_delete=models.CASCADE, blank=True)

    # tehkim_olunmus_montyor = models.ManyToManyField(Montyor,  blank=True, verbose_name='Təhkim olummus montyor')

    def __str__(self):
        return self.skafin_nomresi

    class Meta:
        verbose_name = 'Paylayıcı şkaf'
        verbose_name_plural = 'Paylayıcı şkaflar'


class KomekciPaylayiciSkaf(models.Model):
    skafin_nomresi = models.CharField(max_length=11, blank=True)
    bolme = models.ForeignKey(PaylayiciSkaf, on_delete=models.CASCADE, blank=True)


class KabelNomreleri(models.Model):
    kabelin_nomresi = models.CharField(max_length=11, blank=True, null=True)

    # bagli_oldugu_paylayici_skaf = models.ManyToManyField(PaylayiciSkaf)

    def __str__(self):
        return self.kabelin_nomresi

    class Meta:
        verbose_name = 'Kabel nömrəsi'
        verbose_name_plural = 'Kabel Nomrələri'


class Tehkimolunma(models.Model):
    montyor = models.ForeignKey(Montyor, on_delete=models.CASCADE, blank=True, null=True)
    paylayiciskaf = models.ForeignKey(PaylayiciSkaf, on_delete=models.CASCADE, blank=True, null=True)
    kabel_nomreleri = models.ForeignKey(KabelNomreleri, on_delete=models.CASCADE, blank=True, null=True)


class Nomreler(models.Model):
    KABEL_CEKILME = [
        ('yeralti', 'yeralti'),
        ('hava', 'hava')
    ]

    idare = models.ForeignKey(Idare, on_delete=models.CASCADE)
    nomre = models.CharField(max_length=11, blank=True)
    abonent = models.CharField(max_length=50, blank=True)
    abonentin_elaqe_nomresi = models.CharField(max_length=25)
    sahenin_nomresi = models.CharField(max_length=20, blank=True)
    unvan = models.CharField(max_length=50, blank=True)
    derece = models.CharField(max_length=20, blank=True)
    kabelin_cekilme_veziyyeti = models.CharField(max_length=10, choices=KABEL_CEKILME, blank=True)
    uzunlugu = models.IntegerField(blank=True)
    esas_skaf = models.ForeignKey(PaylayiciSkaf, on_delete=models.CASCADE, blank=True, null=True)
    komekci_skaf = models.ForeignKey(KomekciPaylayiciSkaf, on_delete=models.CASCADE, blank=True, null=True)
    qirmaqlarin_yeri = models.CharField(max_length=25, blank=True)

    # Diğer alanlar

    def __str__(self):
        return self.nomre

    def get_status_display(self):
        return dict(self.STATUS_CHOICES).get(self.olculmus_zedenin_xususiyyeti)

    class Meta:
        verbose_name = 'Nömrə'
        verbose_name_plural = 'Nömrələr'


class ZedelenmelerinQeydiyyatiJurnali(models.Model):
    STATUS_CHOICES = [
        ('qeza', 'qeza'),
        ('kabel qirilib', 'kabel qirilb'),
        ('arassdirilir', 'arassdirilir'),
    ]
    abonent: str = models.ForeignKey(Nomreler, on_delete=models.CASCADE)

    melumatin_daxil_oldugu_vaxt = models.DateTimeField(blank=True, null=True)
    melumatin_mezmunu = models.CharField(max_length=60, blank=True, null=True)

    olculmus_zedenin_xususiyyeti: str = models.CharField(max_length=60, choices=STATUS_CHOICES, blank=True, null=True)
    temire_verilme_vaxti = models.DateTimeField(blank=True, null=True)
    kime_temire_verilib = models.CharField(max_length=50, blank=True, null=True)

    temir_edilib = models.DateTimeField(blank=True, null=True)
    kim_terefinden_temir_edilib = models.CharField(max_length=50, blank=True, null=True)
    temir_edilen_zedenin_xususiyyeti = models.CharField(max_length=60, blank=True, null=True)

    aktivlik_statusu = models.CharField(max_length=11, blank=True, null=True)
    qeyd = models.CharField(max_length=100, null=True, blank=True)
    def __str__(self):
        return str(self.abonent)
    class Meta:
        verbose_name = 'Zədələnmələrin qeydiyatı'
        verbose_name_plural = 'Zədələnmələrin qeydiyyatı'
