from django.contrib import admin

from pr_app.models import Idare, KomekciPaylayiciSkaf, Montyor, PaylayiciSkaf, Nomreler, Tehkimolunma, KabelNomreleri, \
    ZedelenmelerinQeydiyyatiJurnali


# Register your models here.


class TehkimolunmaInline(admin.TabularInline):
    model = Tehkimolunma
    extra = 1


class ZedelenmeInline(admin.TabularInline):
    model = ZedelenmelerinQeydiyyatiJurnali
    extra = 1
    fields = ['melumatin_daxil_oldugu_vaxt', 'melumatin_mezmunu', 'olculmus_zedenin_xususiyyeti',
              'temire_verilme_vaxti', 'kime_temire_verilib', 'temir_edilib', 'kim_terefinden_temir_edilib',
              'temir_edilen_zedenin_xususiyyeti', 'aktivlik_statusu', 'qeyd']
    classes = ('collapse',)


class IdareAdmin(admin.ModelAdmin):
    list_display = ['adi', 'yerlesdiyi_rayon', 'telefon_nomresi']


class MontyorAdmin(admin.ModelAdmin):
    list_display = ['adi', 'idare']
    # filter_horizontal = ('tehkim_olunmus_paylayici_skaflar',)
    inlines = (TehkimolunmaInline,)


class PaylayiciSkafAdmin(admin.ModelAdmin):
    list_display = ['skafin_nomresi', 'mudafie_xetti', 'novu', 'magistral', 'bolme', 'qeyd', 'tabe_oldugu_idare']
    # filter_horizontal = ('montyor', 'kabel_nomresi',)
    inlines = (TehkimolunmaInline,)
    list_filter = ['novu']

class KomekciPaylayiciSkafAdmin(admin.ModelAdmin):
    list_display = ['skafin_nomresi','bolme']

class KabelNomreleriAdmin(admin.ModelAdmin):
    list_display = ['kabelin_nomresi']
    inlines = (TehkimolunmaInline,)


class NomrelerAdmin(admin.ModelAdmin):
    list_display = ['nomre', 'abonent', 'unvan', 'idare', 'esas_skaf', 'komekci_skaf',
                    'abonentin_elaqe_nomresi']
    list_filter = ['esas_skaf', 'idare']
    search_fields = ['nomre']
    list_display_links = ['nomre']
    inlines = (ZedelenmeInline,)


class ZedelenmelerinQeydiyyatiJurnaliAdmin(admin.ModelAdmin):
    list_display = ['abonent', 'melumatin_daxil_oldugu_vaxt', 'melumatin_mezmunu', 'olculmus_zedenin_xususiyyeti',
                    'temire_verilme_vaxti', 'kime_temire_verilib', 'temir_edilib', 'kim_terefinden_temir_edilib',
                    'temir_edilen_zedenin_xususiyyeti', 'temir_edilen_zedenin_xususiyyeti', 'qeyd']

    search_fields = ['abonent__nomre']  # Arama yapmak istediğiniz alanları buraya ekleyin
    raw_id_fields = ['abonent']  # Başlığın açılan pencerede arama kutusu olarak görünmesini sağlar

    list_filter = ['aktivlik_statusu']




admin.site.register(Idare, IdareAdmin)
admin.site.register(Montyor, MontyorAdmin)
admin.site.register(PaylayiciSkaf, PaylayiciSkafAdmin)
admin.site.register(Nomreler, NomrelerAdmin)
admin.site.register(KabelNomreleri, KabelNomreleriAdmin)
admin.site.register(ZedelenmelerinQeydiyyatiJurnali, ZedelenmelerinQeydiyyatiJurnaliAdmin)
admin.site.register(KomekciPaylayiciSkaf, KomekciPaylayiciSkafAdmin)