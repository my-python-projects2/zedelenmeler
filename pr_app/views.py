from django.core.checks import messages
from django.shortcuts import render, get_object_or_404, redirect
from pr_app.forms import ZedelenmelerinQeydiyyatiForm

from pr_app.models import ZedelenmelerinQeydiyyatiJurnali, Nomreler


# Create your views here.
# @login_required
def zedelenme(request):
    # zedelenmeprofile = ZedelenmelerinQeydiyyatiJurnali.objects.all()
    if request.method == 'POST':
        # user_form = Nomreler(request.POST)
        profile_form = ZedelenmelerinQeydiyyatiForm(request.POST)
        if  profile_form.is_valid():
            profile_form.save()
            messages.success(request, 'Sevcccesfuly edites profile')
            return redirect('zedelelenme')
    else:
        profile_form = ZedelenmelerinQeydiyyatiJurnali()

    context = {
        # 'user_form': user_form,
        'profile_form': profile_form,
        # 'zedelenmeprofile':zedelenmeprofile
    }
    return render(request, 'edit_profile.html', context)
